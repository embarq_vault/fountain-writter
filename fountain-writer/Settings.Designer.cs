﻿namespace fountain_writer
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NewSheetNameField = new System.Windows.Forms.TextBox();
            this.NewUserNameField = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SelectClientSecretButton = new System.Windows.Forms.Button();
            this.SelectTemplateButton = new System.Windows.Forms.Button();
            this.CurrentTemplateLable = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NewSheetNameField);
            this.groupBox1.Controls.Add(this.NewUserNameField);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 99);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sheet name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "User name";
            // 
            // NewSheetNameField
            // 
            this.NewSheetNameField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NewSheetNameField.Location = new System.Drawing.Point(111, 62);
            this.NewSheetNameField.Name = "NewSheetNameField";
            this.NewSheetNameField.Size = new System.Drawing.Size(137, 23);
            this.NewSheetNameField.TabIndex = 1;
            // 
            // NewUserNameField
            // 
            this.NewUserNameField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NewUserNameField.Location = new System.Drawing.Point(111, 23);
            this.NewUserNameField.Name = "NewUserNameField";
            this.NewUserNameField.Size = new System.Drawing.Size(137, 23);
            this.NewUserNameField.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SelectClientSecretButton);
            this.groupBox2.Controls.Add(this.SelectTemplateButton);
            this.groupBox2.Controls.Add(this.CurrentTemplateLable);
            this.groupBox2.Location = new System.Drawing.Point(12, 117);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 97);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application Settings";
            // 
            // SelectClientSecretButton
            // 
            this.SelectClientSecretButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.SelectClientSecretButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SelectClientSecretButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectClientSecretButton.Location = new System.Drawing.Point(128, 47);
            this.SelectClientSecretButton.Name = "SelectClientSecretButton";
            this.SelectClientSecretButton.Size = new System.Drawing.Size(120, 44);
            this.SelectClientSecretButton.TabIndex = 6;
            this.SelectClientSecretButton.Text = "Choose new client secret file";
            this.SelectClientSecretButton.UseVisualStyleBackColor = false;
            this.SelectClientSecretButton.Click += new System.EventHandler(this.SelectClientSecretButton_Click);
            // 
            // SelectTemplateButton
            // 
            this.SelectTemplateButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.SelectTemplateButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SelectTemplateButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectTemplateButton.Location = new System.Drawing.Point(6, 47);
            this.SelectTemplateButton.Name = "SelectTemplateButton";
            this.SelectTemplateButton.Size = new System.Drawing.Size(120, 44);
            this.SelectTemplateButton.TabIndex = 4;
            this.SelectTemplateButton.Text = "Choose another template";
            this.SelectTemplateButton.UseVisualStyleBackColor = false;
            this.SelectTemplateButton.Click += new System.EventHandler(this.SelectTemplateButton_Click);
            // 
            // CurrentTemplateLable
            // 
            this.CurrentTemplateLable.AutoEllipsis = true;
            this.CurrentTemplateLable.AutoSize = true;
            this.CurrentTemplateLable.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentTemplateLable.Location = new System.Drawing.Point(7, 19);
            this.CurrentTemplateLable.Name = "CurrentTemplateLable";
            this.CurrentTemplateLable.Size = new System.Drawing.Size(171, 21);
            this.CurrentTemplateLable.TabIndex = 3;
            this.CurrentTemplateLable.Text = "Current template name";
            // 
            // ApplyButton
            // 
            this.ApplyButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ApplyButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ApplyButton.Location = new System.Drawing.Point(18, 220);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(120, 23);
            this.ApplyButton.TabIndex = 2;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = false;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.CloseButton.Location = new System.Drawing.Point(140, 220);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(120, 23);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // Settings
            // 
            this.AcceptButton = this.ApplyButton;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(278, 256);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NewSheetNameField;
        private System.Windows.Forms.TextBox NewUserNameField;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button SelectTemplateButton;
        private System.Windows.Forms.Label CurrentTemplateLable;
        private System.Windows.Forms.Button SelectClientSecretButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CloseButton;
    }
}