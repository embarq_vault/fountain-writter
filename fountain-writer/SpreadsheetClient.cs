﻿using System;
using System.IO;
using System.Collections.Generic;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Newtonsoft.Json;

namespace fountain_writer
{
    class SpreadsheetClient
    {
        private string Id { get; set; }
        private SheetsService Service { get; set; }
        private static string BackupFileDefaultPath
        {
            get
            {
                return Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    Path.ChangeExtension("backup", "json"));
            }
        }

        /// <summary>
        /// General store. Key - sheet name, Value - sheet content
        /// </summary>
        public Dictionary<string, Sheet> Sheets { get; set; }

        /// <summary>
        /// List of sheet names
        /// </summary>
        public List<string> Titles { get; set; }

        public SpreadsheetClient(string spreadsheetName)
        {
            if (string.IsNullOrEmpty(spreadsheetName) || 
                string.IsNullOrWhiteSpace(spreadsheetName))
            {
                throw new ArgumentException(
                    "Missing Google Sheet name", 
                    "spreadsheetName");
            }
            
            this.Titles = new List<string>();
            this.Sheets = new Dictionary<string, Sheet>();
            
            foreach (var sheet in GetSheets(spreadsheetName))
            {
                string sheetName = sheet.Properties.Title;

                if (sheet.Data.Count.Equals(0))
                {
                    Sheets.Add(sheetName, new Sheet());
                }

                Titles.Add(sheetName);
                Sheets.Add(
                    sheetName,
                    new Sheet(sheet.Data[0].RowData));
            }

            Export(null);
        }

        public SpreadsheetClient()
        {
            Sheets = new Dictionary<string, Sheet>();
            Titles = new List<string>();
            Service = InitializeService(new Credentials());
        }

        private SheetsService InitializeService(Credentials credentials)
        {
            return new SheetsService(
                new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credentials.Data,
                    ApplicationName = credentials.AppName
                });
        }

        /// <summary>
        /// Get spreadsheet content from Google Drive
        /// </summary>
        /// <returns>List of sheets</returns>
        private IList<Google.Apis.Sheets.v4.Data.Sheet> GetSheets(string spreadsheetName)
        {
            var credentials = new Credentials();
            var driveClient = new DriveClient(credentials);

            Service = InitializeService(credentials);

            try
            {
                Id = driveClient.GetFileByName(spreadsheetName);
            }
            catch (FileNotFoundException exception)
            {
                throw exception;
            }

            var request = Service.Spreadsheets.Get(Id);
            request.Fields = @"sheets.data.rowData.values.formattedValue,sheets.properties.title";
            var response = request.Execute();
            return response.Sheets;
        }

        public async void Export(string path)
        {
            if (!IsValidPath(path))
            {
                path = BackupFileDefaultPath;
            }

            string json = JsonConvert.SerializeObject(this, Formatting.None);

            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    await writer.WriteAsync(json);
                }
            }
            catch
            {
                var wait = new System.Timers.Timer();
                wait.AutoReset = true;
                wait.Interval = 1e3;
                wait.Elapsed += (obj, args) => Export(path);
            }
        }

        public static SpreadsheetClient Import(string path)
        {
            
            if (!IsValidPath(path))
            {
                path = BackupFileDefaultPath;
            }

            string json = "";

            using (StreamReader reader = new StreamReader(path))
            {
                json = reader.ReadToEnd();
            }
            
            return JsonConvert.DeserializeObject<SpreadsheetClient>(json);
        }

        private static bool IsValidPath(string path)
        {
            if (string.IsNullOrEmpty(path) || string.IsNullOrWhiteSpace(path))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
