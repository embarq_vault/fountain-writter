﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using TemplateEngine.Docx;

namespace fountain_writer
{
    class TemplateEngineClient
    {
        private static string CurrentDirectory
        {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }
        private static string DefaultTemplate
        {
            get
            {
                return Path.Combine(
                    CurrentDirectory, 
                    Path.ChangeExtension("template", "docx"));
            }
        }

        /// <summary>
        /// Templates row keys by appending order number. 
        /// It's necessary for dual templating
        /// </summary>
        /// <param name="source">Initial row</param>
        /// <param name="order">Entry order</param>
        /// <returns>Templated row</returns>
        private static Dictionary<string, string> TemplateRow(Dictionary<string, string> source, int order)
        {
            var result = new Dictionary<string, string>();
            
            foreach(var item in source)
            {
                result.Add($"{item.Key}{order}", item.Value);
            }

            return result;
        }

        /// <param name="templatePath">Path to template</param>
        /// <param name="sheet">Data to do template of</param>
        /// <returns>List of exceptions</returns>
        public async static Task<List<Exception>> TemplateAsync(string templatePath, Sheet sheet)
        {
            var errors = new List<Exception>();
            string outputDirectory = Path.Combine(CurrentDirectory, "output");

            templatePath = File.Exists(templatePath) ? templatePath : DefaultTemplate;

            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            
            await Task.Factory.StartNew(() =>
            {
                var list = sheet.Data.ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    Dictionary<string, string> firstEntry;
                    Dictionary<string, string> secondEntry;
                    string entryKey = "";

                    Trace.WriteLineIf(i + 1 >= list.Count, "Hey yout");

                    if (i + 1 >= list.Count)
                    {
                        firstEntry = TemplateRow(list[i].Value, 0);
                        secondEntry = new Dictionary<string, string>();

                        entryKey = list[i].Key;
                    }
                    else
                    {
                        firstEntry = TemplateRow(list[i].Value, 0);
                        secondEntry = TemplateRow(list[i + 1].Value, 1);

                        entryKey = $"{ list[i].Key}-{ list[i + 1].Key}";
                    }

                    Template(
                        entryKey, 
                        ref templatePath, 
                        ref outputDirectory, 
                        firstEntry, 
                        secondEntry);

                    i++;
                }
            });

            return errors;
        }

        /// <summary>
        /// Template firstEntry and secondEntry to the one Word template
        /// </summary>
        /// <param name="entryKey">File name</param>
        /// <param name="templatePath">Path to template</param>
        /// <param name="outputDirectory">Destination folder</param>
        /// <param name="firstEntry">First data subset</param>
        /// <param name="secondEntry">Second data subset</param>
        private static void Template(string entryKey, ref string templatePath, ref string outputDirectory, Dictionary<string, string> firstEntry, Dictionary<string, string> secondEntry)
        {
            string filename = Path.ChangeExtension(entryKey.Replace(".", "-"), "docx");
            string currentPath = Path.Combine(outputDirectory, filename);

            File.Delete(currentPath);
            File.Copy(templatePath, currentPath);

            List<FieldContent> firstEntryContent = firstEntry.Select(
                item => new FieldContent(item.Key, item.Value)).ToList();

            List<FieldContent> secondEntryContent = secondEntry.Select(
                item => new FieldContent(item.Key, item.Value)).ToList();

            firstEntryContent.AddRange(secondEntryContent);

            var valuesToFill = new Content(firstEntryContent.ToArray());

            using (var outputDocument = new TemplateProcessor(currentPath)
                                            .SetRemoveContentControls(true))
            {
                outputDocument.SetNoticeAboutErrors(false);
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }
        }
    }
}
