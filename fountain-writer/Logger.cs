﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace fountain_writer
{
    class Logger
    {
        private string Path;
        private List<Exception> Exceptions;

        public Logger()
        {
            Path = AppDomain.CurrentDomain.BaseDirectory;
            Exceptions = new List<Exception>();
        }

        public void Write(Exception e)
        {
            Exceptions.Add(e);
        }

        public void Save()
        {
            string filename = string.Concat(
                Path, 
                string.Format("{0}_{1:dd.MM.yyy}.log", 
                AppDomain.CurrentDomain.FriendlyName, 
                DateTime.Now));

            string format = "[{0:dd.MM.yyy HH:mm:ss.fff}] {1}\r\n";
            StringBuilder exceptionsStringBuilder = new StringBuilder();

            foreach (Exception e in Exceptions)
            {
                exceptionsStringBuilder.AppendFormat(
                    format,
                    DateTime.Now,
                    e.Message);
            }
            
            File.AppendAllText(
                filename, 
                exceptionsStringBuilder.ToString(), 
                Encoding.GetEncoding("utf-8"));
        }
    }
}
