﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Google.Apis.Sheets.v4.Data;

namespace fountain_writer
{
    class Sheet
    {
        /// <summary>
        /// Key - Means sheet row. Identifier of data entry,
        /// Value - Means sheet column. Dictionary[FieldName, FieldValue]
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> Data { get; set; }

        /// <summary>
        /// List of avalible sheet captions
        /// </summary>
        public List<string> Captions { get; }
        
        /// <summary>
        /// Initialize sheet data by Google.RowData
        /// </summary>
        /// <param name="data">Sheet content</param>
        public Sheet(IList<RowData> data)
        {
            Data = new Dictionary<string, Dictionary<string, string>>();

            var keyCollisions = new List<Exception>();
            var captions = data[0].Values.Select(
                caption => caption.FormattedValue);

            data.RemoveAt(0);

            Captions = new List<string>(captions);

            foreach (RowData row in data)
            {
                var subset = new Dictionary<string, string>();
                foreach (CellData cell in row.Values)
                {
                    int captionIndex = row.Values.IndexOf(cell);
                    string cellLabel = Captions[captionIndex];
                    string cellValue = "";

                    try
                    {
                        cellValue = cell.FormattedValue;
                    }
                    catch (NullReferenceException)
                    {
                        cellValue = "";
                    }

                    subset.Add(cellLabel, cellValue);
                }
                var keyHash = subset[Captions[0]];

                try
                {
                    Data.Add(keyHash, subset);
                }
                // When keys repeats
                catch (ArgumentException exception)
                {
                    Data.Remove(keyHash);
                    Data.Add(keyHash, subset);

                    exception = new ArgumentException(
                        $"Data key collision on key: {subset[Captions[0]]}");
                    keyCollisions.Add(exception);
                }
            }        
            
            if (keyCollisions.Count > 0)
            {
                foreach (Exception conflict in keyCollisions)
                {
                    Program.Log.Write(conflict);
                }
            }   
        }

        /// <summary>
        /// Initialize sheet data by custom data. Expected call by TemplateEngine
        /// </summary>
        /// <param name="data">Sheet content</param>
        public Sheet(List<List<string>> data)
        {
            Data = new Dictionary<string, Dictionary<string, string>>();
            Captions = new List<string>(data[0]);

            var keyCollisions = new List<Exception>();
            
            data.RemoveAt(0);   // Remove captions

            foreach (var row in data)
            {
                var subset = new Dictionary<string, string>();
                foreach (string cell in row)
                {
                    int captionIndex = row.IndexOf(cell);
                    string cellLabel = Captions[captionIndex];
                    string cellValue = string.IsNullOrEmpty(cell) ? "" : cell;

                    subset.Add(cellLabel, cellValue);
                }

                var keyHash = $"{subset[Captions[1]]} - {subset[Captions[2]]}";

                try
                {
                    Data.Add(keyHash, subset);
                }
                // Occurs when a key repeats
                catch (ArgumentException exception)
                {
                    Data.Remove(keyHash);
                    Data.Add(keyHash, subset);

                    exception = new ArgumentException($"Data key collision on key: {subset[Captions[0]]}");
                    keyCollisions.Add(exception);
                }
            }

            if (keyCollisions.Count > 0)
            {
                foreach (Exception conflict in keyCollisions)
                {
                    Program.Log.Write(conflict);
                }
            }
        }

        public Sheet()
        {
            Data = new Dictionary<string, Dictionary<string, string>>();
            Captions = new List<string>();
        }
        
        public async void CheckNullsAsync()
        {
            await new Task(new Action(CheckNulls));
        }

        public void CheckNulls()
        {
            Data.Values.Select(
                row => row.Values.Select(
                    col => string.IsNullOrEmpty(col) ? "" : col));
        }
    }
}
