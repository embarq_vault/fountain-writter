﻿using System;
using System.Windows.Forms;

namespace fountain_writer
{
    public partial class Register : Form
    {
        public string UserName
        {
            get { return textBox1.Text; }
        }

        public string SheetName
        {
            get { return textBox2.Text; }
        }

        public Register()
        {
            InitializeComponent();
        }

        public static DialogResult Show(out string userName, out string sheetName)
        {
            Register registerDialog = null;
            DialogResult dialogResult = DialogResult.None;

            using (registerDialog = new Register())
            {
                dialogResult = registerDialog.ShowDialog();
                userName = registerDialog.UserName;
                sheetName = registerDialog.SheetName;
            }

            return dialogResult;
        }
        
        private void Register_KeyUp(object sender, KeyEventArgs e)
        {
            button1.Enabled =
                !string.IsNullOrEmpty(textBox1.Text) &&
                !string.IsNullOrEmpty(textBox2.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
