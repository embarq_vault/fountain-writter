﻿using System;
using System.Collections.Generic;
using Google.Apis.Drive.v3;
using Google.Apis.Services;

namespace fountain_writer
{
    class DriveClient
    {
        private DriveService Service;

        public DriveClient(Credentials credentials)
        {
            Service = new DriveService(
                new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credentials.Data,
                    ApplicationName = credentials.AppName
                });
        }

        /// <summary>
        /// Returns id of requested file
        /// </summary>
        /// <param name="filename">Request file by name</param>
        /// <returns>id of requested file</returns>
        public string GetFileByName(string filename)
        {
            IList<Google.Apis.Drive.v3.Data.File> files = null;
            var request = Service.Files.List();
            request.Q = $"name = '{filename}'";

            files = request.ExecuteAsync().Result.Files;

            if (files.Count < 1)
            {
                throw new System.IO.FileNotFoundException(
                    $"File \"{filename}\" not present on your Drive");
            }
            else if (files.Count > 1)
            {
                throw new System.IO.FileNotFoundException(
                    $"There's more then one file's \"{filename}\" entry present on your drive");
            }

            return files[0].Id;
        }
    }
}
