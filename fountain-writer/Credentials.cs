﻿using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;

namespace fountain_writer
{
    class Credentials
    {
        /// <summary>
        /// Auth credentials data
        /// </summary>
        public UserCredential Data { get; }
        private class ClientSecret
        {
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public ClientSecret() { }
            public ClientSecret(string client_id, string client_secret)
            {
                this.client_id = client_id;
                this.client_secret = client_secret;
            }
        }

        public string AppName { get; } = Main.Registry.DefaultAppRegistryKey;

        public Credentials()
        {
            var clientSecrets = new ClientSecrets();

            clientSecrets.ClientId = Main.Registry.ClientId;
            clientSecrets.ClientSecret = Main.Registry.ClientSecret;

            string[] scopes = {
                SheetsService.Scope.SpreadsheetsReadonly,
                DriveService.Scope.Drive
            };

            string credPath = Environment.GetFolderPath(
                Environment.SpecialFolder.Personal);

            credPath = Path.Combine(
                credPath,
                "chdu.documentator.credentials");

            this.Data = GoogleWebAuthorizationBroker.AuthorizeAsync(
                clientSecrets, 
                scopes,
                Main.Registry.UserName,
                CancellationToken.None,
                new FileDataStore(credPath, true)
            ).Result;
        }

        /// <summary>
        ///  Write client secret data to registry from separate json file
        /// </summary>
        /// <param name="path">Path to client secret file</param>
        public static void RegisterClientSecret(string path)
        {
            ClientSecret clientSecret = null;

            try
            {
                clientSecret = GetClientSecret(path);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            foreach (var prop in typeof(ClientSecret).GetProperties())
            {
                string key = prop.Name;
                var value = prop.GetValue(clientSecret).ToString();

                Main.Registry.AppKey.SetValue(key, value);
            }
        }

        /// <summary>
        /// Reads and deserializes client secret from *.json file
        /// </summary>
        /// <param name="path">Path to client secret</param>
        /// <returns>ClientSecrert instance</returns>
        private static ClientSecret GetClientSecret(string path)
        {
            string content = "";
            try
            {
                content = File.ReadAllText(path);
            }
            catch (ArgumentNullException exception)
            {
                throw exception;
            }
            
            return Newtonsoft.Json.Linq.JObject
                .Parse(content).First.First.ToObject<ClientSecret>();
        }
    }
}
