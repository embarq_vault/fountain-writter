﻿using System;
using System.Windows.Forms;

namespace fountain_writer
{
    static class Program
    {
        public static Logger Log { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Log = new Logger();

            Application.ApplicationExit += (obj, args) => Log.Save();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}
