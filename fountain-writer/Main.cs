﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.IO;

// Post-build: "$(SolutionDir)\ILMerge\merge_all.bat" "$(SolutionDir)" "$(TargetPath)" $(ConfigurationName) /targetplatform:v4,”%ProgramFiles%\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5”

namespace fountain_writer
{
    public partial class Main : Form
    {
        delegate void PrintSheetCallback();
        private bool IsConnected
        {
            get
            {
                try
                {
                    using (var client = new System.Net.WebClient())
                    {
                        using (var stream = client.OpenRead("http://www.google.com"))
                        {
                            return true;
                        }
                    }
                }
                catch
                {
                    return false;
                }
            }
        }
        private SpreadsheetClient Spreadsheet { get; set; }
        private string CurrentSheet { get; set; }
        public static string CurrentTemplatePath { get; set; }
        public static string CurrentClientSecretPath { get; set; }
        public static AppRegistry Registry { get; } = new AppRegistry(null);

        public Main()
        {
            InitializeComponent();

            if (Registry.FirstFun)
            {
                RegisterUser(null);
                RegisterApp(null);
                CopyTemplate(null);
            }

            InitializeContentAsync();
        }

        private void CopyTemplate(string path)
        {
            bool overwrite = true;

            // When Main reconfigurates
            if (string.IsNullOrEmpty(path))
            {
                path = OpenFileDialog("Select your Template", "Microsoft Word|*.docx");

                // When user closes dialog
                if (string.IsNullOrEmpty(path))
                {
                    Program.Log.Write(new NullReferenceException("File isn't selected"));
                    Environment.Exit(13);
                }
            }

            string fileName = Path.GetFileName(path);
            string destination = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

            try
            {
                File.Copy(path, destination, overwrite);
            }
            catch (IOException exception) // File uses by another process
            {
                Program.Log.Write(exception);
                System.Timers.ElapsedEventHandler handleEllapsed =
                    (obj, args) => CopyTemplate(path);

                var wait = new System.Timers.Timer();
                wait.Interval = 4e3;
                wait.AutoReset = true;
                wait.Elapsed += handleEllapsed;
                wait.Start();
            }

            Registry.TemplatePath = destination;
        }

        private void RegisterUser(SettingConfig config)
        {
            if (config == null)
            {
                string userName, spreadsheetName;
                DialogResult registerDialogResult = Register.Show(out userName, out spreadsheetName);

                switch (registerDialogResult)
                {
                    case DialogResult.OK:
                        Registry.SpreadsheetName = spreadsheetName;
                        Registry.UserName = userName;
                        break;
                    case DialogResult.Cancel:
                        Environment.Exit(2);
                        break;
                }
            }
            else
            {
                Registry.SpreadsheetName = config.SpreadsheetName;
                Registry.UserName = config.UserName;
            }
        }

        /// <summary>
        /// Add client secret info to windows registry from separate json file
        /// </summary>
        private void RegisterApp(string clientSecretPath)
        {
            if (string.IsNullOrEmpty(clientSecretPath))
            {
                clientSecretPath = OpenFileDialog("Select your Google API client secret file", "JSON|*.json");
            }

            Registry.ClientSecretPath = clientSecretPath;

            // Toggle registry field(about first run of program on current PC)
            Registry.FirstFun = false;

            try
            {
                Credentials.RegisterClientSecret(clientSecretPath);
            }
            catch (Exception exception) // if user cancel OpenFileDialog
            {
                Program.Log.Write(exception);
                Environment.Exit(2);
            }
        }


        private void Tune()
        {
            var currentSettings = new SettingConfig(
                Registry.UserName,
                Registry.SpreadsheetName,
                Registry.TemplatePath,
                Registry.ClientSecretPath);

            var newSettings = new SettingConfig();

            DialogResult settingsWindowResult = Settings.Show(
                currentSettings, ref newSettings);

            switch (settingsWindowResult)
            {
                case DialogResult.OK:
                    RegisterUser(newSettings);
                    RegisterApp(newSettings.ClientSecretPath);
                    CopyTemplate(newSettings.TemplatePath);
                    break;
                case DialogResult.Cancel:
                    Environment.Exit(2);
                    break;
            }
        }

        /// <summary>
        /// Opens file dialog
        /// </summary>
        /// <param name="title">Dialog title</param>
        /// <param name="filter">File type</param>
        /// <returns>Path to file</returns>
        public static string OpenFileDialog(string title, string filter)
        {
            var dialog = new OpenFileDialog();

            dialog.Filter = filter;
            dialog.Title = title;
            dialog.Multiselect = false;

            string path = dialog.ShowDialog() == DialogResult.OK ? dialog.FileName : null;

            dialog.Dispose();

            return path;
        }

        private void InitializeContentAsync()
        {
            CurrentSheetLabel.Text = "Loading...";
            var backgroundWorker = new System.ComponentModel.BackgroundWorker();

            // Things to do async work
            backgroundWorker.DoWork += (obj, args) => InitializeContent();

            // Things to do work after async operations would be done
            backgroundWorker.RunWorkerCompleted += (obj, args) => PrintSheet();
            backgroundWorker.RunWorkerCompleted += (obj, args) => CurrentSheetLabel.Text = CurrentSheet;
            backgroundWorker.RunWorkerCompleted += (obj, args) => Spreadsheet.Export(null);

            backgroundWorker.RunWorkerAsync();
        }

        private void InitializeContent()
        {
            if (!this.IsConnected)
            {
                Spreadsheet = SpreadsheetClient.Import(null);
            }
            else
            {
                try
                {
                    Spreadsheet = new SpreadsheetClient(Registry.SpreadsheetName);
                    foreach (var sheet in Spreadsheet.Sheets)
                    {
                        sheet.Value.CheckNullsAsync();
                    }
                }
                catch (FileNotFoundException exception)
                {
                    Program.Log.Write(exception);
                    MessageBox.Show(exception.Message);
                    Environment.Exit(2);
                }
                catch (ArgumentException exception)
                {
                    Program.Log.Write(exception);
                    MessageBox.Show(exception.Message);
                }
            }

            CurrentSheet = Spreadsheet?.Titles[0];
        }

        private void PrintSheet()
        {
            Sheet sheet = Spreadsheet.Sheets[CurrentSheet];

            SheetView.Clear();

            if (SheetView.InvokeRequired)
            {
                PrintSheetCallback callback = new PrintSheetCallback(PrintSheet);
                Invoke(callback, new object[] { });
            }
            else
            {
                foreach (string caption in sheet.Captions)
                {
                    var header = new ColumnHeader();

                    header.Text = caption;
                    header.TextAlign = HorizontalAlignment.Center;

                    SheetView.Columns.Add(header);
                }

                foreach (var row in sheet.Data.Values)
                {
                    SheetView.Items.Add(new ListViewItem(row.Values.ToArray()));
                }

                SheetView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                SheetView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private async void FileExportAllItem_Click(object sender, EventArgs e)
        {
            FileExportAllItem.Enabled = false;
            CurrentSheetLabel.Text = "Templating...";
            await TemplateEngineClient.TemplateAsync(Registry.TemplatePath, Spreadsheet.Sheets[CurrentSheet]);
            FileExportAllItem.Enabled = true;
            CurrentSheetLabel.Text = CurrentSheet;
        }

        /// <summary>
        /// Creates new Sheet from selected SheetView items and templates that
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FileExportSelectionMenuItem_Click(object sender, EventArgs e)
        {
            FileExportAllItem.Enabled = false;
            CurrentSheetLabel.Text = "Templating...";

            var currentSheet = Spreadsheet.Sheets[CurrentSheet];
            var sheetData = new List<List<string>>();

            sheetData.Add(currentSheet.Captions);

            foreach (ListViewItem item in SheetView.CheckedItems.OfType<ListViewItem>().ToArray())
            {
                var entireRow = item.SubItems.Cast<ListViewItem.ListViewSubItem>()
                    .Select(subItem => subItem.Text)
                    .ToList();
                sheetData.Add(entireRow);
            }

            var sheet = new Sheet(sheetData);

            await TemplateEngineClient.TemplateAsync(null, sheet);

            FileExportAllItem.Enabled = true;
            CurrentSheetLabel.Text = CurrentSheet;
        }

        private void SettingsMenuItem_Click(object sender, EventArgs e)
        {
            Tune();
        }

        private void RefreshMenuItem_Click(object sender, EventArgs e)
        {
            InitializeContentAsync();
        }
    }
}
