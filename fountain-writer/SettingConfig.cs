﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fountain_writer
{
    /// <summary>
    /// Provides user settings
    /// </summary>
    public class SettingConfig
    {
        public string UserName { get; set; }
        public string SpreadsheetName { get; set; }
        public string TemplatePath { get; set; }
        public string ClientSecretPath { get; set; }

        public SettingConfig() 
        {
			UserName = null;
			SpreadsheetName = null;
			TemplatePath = null;
			ClientSecretPath = null;
        }

        public SettingConfig(string userName, string spreadsheetName, string templatePath, string clientSecretPath)
        {
			this.UserName = userName;
			this.SpreadsheetName = spreadsheetName;
			this.TemplatePath = templatePath;
			this.ClientSecretPath = clientSecretPath;
        }
    }
}
