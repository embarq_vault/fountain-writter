﻿using System;
using Microsoft.Win32;

namespace fountain_writer
{
    /// <summary>
    /// Registry helper class
    /// </summary>
    public class AppRegistry
    {
        // default key value of app registry
        private static string _defaultAppRegistryKey { get; } = @"Documentator (CHDU FCS)";
        public string DefaultAppRegistryKey
        {
            get { return _defaultAppRegistryKey; }
        }
        private static string FirstRunKey { get; } = "first-run";
        public bool FirstFun
        {
            get
            {
                bool value;
                try
                {
                    string registryValue = AppKey.GetValue(FirstRunKey).ToString();
                    value = Convert.ToBoolean(registryValue);
                }
                catch(NullReferenceException)   // if value isn't exist
                {
                    value = true;
                }
                return value;
            }
            set
            {
                AppKey.SetValue(FirstRunKey, value);
            }
        }

        public string SpreadsheetName 
        { 
            get { return GetStringValue("sheet-name"); }
            set { AppKey.SetValue("sheet-name", value); } 
        }
        public string UserName 
        { 
            get { return GetStringValue("username"); } 
            set { AppKey.SetValue("username", value); }
        }
        public string TemplatePath 
        { 
            get { return GetStringValue("template-path"); } 
            set { AppKey.SetValue("template-path", value); }
        }
        public string ClientSecretPath
        { 
            get { return GetStringValue("client-secret-path"); } 
            set { AppKey.SetValue("client-secret-path", value); }
        }
        public string ClientId 
        { 
            get { return GetStringValue("client_id"); } 
        }
        public string ClientSecret 
        { 
            get { return GetStringValue("client_secret"); } 
        }
        public RegistryKey AppKey { get; }

        public AppRegistry(string appRegistryKey)
        {
            string registryKey = string.IsNullOrEmpty(appRegistryKey) ?
                    _defaultAppRegistryKey : appRegistryKey;
            
            AppKey = Registry.CurrentUser.CreateSubKey(registryKey);
        }

        private string GetStringValue(string key)
        {
            return AppKey.GetValue(key).ToString();
        }

        ~AppRegistry()
        {
            AppKey.Close();
        }
    }
}
