﻿using System;
using System.Windows.Forms;

namespace fountain_writer
{
    public partial class Settings : Form
    {
        private string NewUserName
        {
            get
            {
                return NewUserNameField.Text;
            }
        }
        private string NewSpreadsheetName
        {
            get
            {
                return NewSheetNameField.Text;
            }
        }
        private string NewTemplatePath { get; set; }
        private string NewClientSecretPath { get; set; }
        public Settings()
        {
            InitializeComponent();
        }

        public static DialogResult Show(SettingConfig oldSettings, ref SettingConfig newSettings)
        {
            var newSettingsWindow = new Settings();
            var dialogResult = DialogResult.OK;

            newSettingsWindow.NewUserNameField.Text = oldSettings.UserName;
            newSettingsWindow.NewSheetNameField.Text = oldSettings.SpreadsheetName;
            newSettingsWindow.CurrentTemplateLable.Text = oldSettings.TemplatePath;

            using (newSettingsWindow)
            {
                newSettingsWindow.ShowDialog();
                newSettings.UserName = newSettingsWindow.NewUserName;
                newSettings.SpreadsheetName = newSettingsWindow.NewSpreadsheetName;

                if (newSettingsWindow.NewTemplatePath == oldSettings.TemplatePath ||
                    newSettingsWindow.NewTemplatePath == null)
                {
                    newSettings.TemplatePath = oldSettings.TemplatePath;
                }
                else
                {
                    newSettings.TemplatePath = newSettingsWindow.NewTemplatePath;
                }

                if (newSettingsWindow.NewClientSecretPath == oldSettings.TemplatePath ||
                    newSettingsWindow.NewClientSecretPath == null)
                {
                    newSettings.ClientSecretPath = oldSettings.ClientSecretPath;
                }
                else
                {
                    newSettings.ClientSecretPath = newSettingsWindow.NewClientSecretPath;
                }
            }

            return dialogResult;
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectTemplateButton_Click(object sender, EventArgs e)
        {
            NewTemplatePath = Main.OpenFileDialog("Select your Template", "Microsoft Word|*.docx");
        }

        private void SelectClientSecretButton_Click(object sender, EventArgs e)
        {
            NewClientSecretPath = Main.OpenFileDialog("Select your Google API client secret file", "JSON|*.json");
        }
    }
}
