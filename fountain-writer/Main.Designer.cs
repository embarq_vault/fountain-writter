﻿namespace fountain_writer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.SheetView = new System.Windows.Forms.ListView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.CurrentSheetLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileExportSelectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileExportAllItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.FileExitItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SheetView
            // 
            resources.ApplyResources(this.SheetView, "SheetView");
            this.SheetView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SheetView.CheckBoxes = true;
            this.SheetView.FullRowSelect = true;
            this.SheetView.GridLines = true;
            this.SheetView.Name = "SheetView";
            this.SheetView.UseCompatibleStateImageBehavior = false;
            this.SheetView.View = System.Windows.Forms.View.Details;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrentSheetLabel});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // CurrentSheetLabel
            // 
            this.CurrentSheetLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CurrentSheetLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.CurrentSheetLabel.Name = "CurrentSheetLabel";
            resources.ApplyResources(this.CurrentSheetLabel, "CurrentSheetLabel");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.RefreshMenuItem,
            this.SettingsMenuItem,
            this.AboutMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileExportSelectionMenuItem,
            this.FileExportAllItem,
            this.toolStripSeparator1,
            this.FileExitItem});
            this.FileMenuItem.Name = "FileMenuItem";
            resources.ApplyResources(this.FileMenuItem, "FileMenuItem");
            // 
            // FileExportSelectionMenuItem
            // 
            this.FileExportSelectionMenuItem.Name = "FileExportSelectionMenuItem";
            resources.ApplyResources(this.FileExportSelectionMenuItem, "FileExportSelectionMenuItem");
            this.FileExportSelectionMenuItem.Click += new System.EventHandler(this.FileExportSelectionMenuItem_Click);
            // 
            // FileExportAllItem
            // 
            this.FileExportAllItem.Name = "FileExportAllItem";
            resources.ApplyResources(this.FileExportAllItem, "FileExportAllItem");
            this.FileExportAllItem.Click += new System.EventHandler(this.FileExportAllItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // FileExitItem
            // 
            this.FileExitItem.Name = "FileExitItem";
            resources.ApplyResources(this.FileExitItem, "FileExitItem");
            // 
            // SettingsMenuItem
            // 
            this.SettingsMenuItem.Name = "SettingsMenuItem";
            resources.ApplyResources(this.SettingsMenuItem, "SettingsMenuItem");
            this.SettingsMenuItem.Click += new System.EventHandler(this.SettingsMenuItem_Click);
            // 
            // AboutMenuItem
            // 
            this.AboutMenuItem.Name = "AboutMenuItem";
            resources.ApplyResources(this.AboutMenuItem, "AboutMenuItem");
            // 
            // RefreshMenuItem
            // 
            this.RefreshMenuItem.Name = "RefreshMenuItem";
            resources.ApplyResources(this.RefreshMenuItem, "RefreshMenuItem");
            this.RefreshMenuItem.Click += new System.EventHandler(this.RefreshMenuItem_Click);
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.SheetView);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView SheetView;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel CurrentSheetLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FileExportSelectionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FileExportAllItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem FileExitItem;
        private System.Windows.Forms.ToolStripMenuItem AboutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SettingsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RefreshMenuItem;
    }
}

